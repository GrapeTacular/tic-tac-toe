/* Minimax With Alpha-Beta Pruning
****************************************************************************************/

function makeMinimax(isTerminalNode, evaluateNode, getChildNodes, maximum) {
	function minimax(node, depth, alpha, beta, maximizer) {
    if (depth <= 0 || isTerminalNode(node)) {
      return evaluateNode(node);
    }

    var childNodes = getChildNodes(node);
    var childNodesCount = childNodes.length;
    var score = 0;

    if (maximizer) {
      score = -maximum;

      for (var i = 0; i < childNodesCount; ++i) {
        score = Math.max(score, minimax(childNodes[i], depth - 1, alpha, beta, false));
        alpha = Math.max(alpha, score);

        if (beta <= alpha) {
          break;
        }
      }

    } else {
      score = maximum;

      for (var i = 0; i < childNodesCount; ++i) {
        score = Math.min(score, minimax(childNodes[i], depth - 1, alpha, beta, true));
        beta = Math.min(beta, score);

        if (beta <= alpha) { break; }
      }
    }
    
		return score;
  }

  return minimax;
}
