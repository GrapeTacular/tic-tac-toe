/* Tic Tac Toe Functions
****************************************************************************************/

function makeTicTacToeFuncs(human, computer, first, second) {
  

  /* Helpers
  ****************************************************************************************/
  
  function get3RowCount(node, pro, ant) {
    var proCount = 0;
    var antCount = 0;

    for (var r = 0; r < 3; ++r) {
      if (node[r][0] === node[r][1] && node[r][1] === node[r][2]) {
        if (node[r][1] === pro) {
          proCount += 1;
        } else if (node[r][1] === ant) {
          antCount += 1;
        }
      }
    }

    return {
      proCount: proCount,
      antCount: antCount
    };
  }

  function get3ColCount(node, pro, ant) {
    var proCount = 0;
    var antCount = 0;

    for (var c = 0; c < 3; ++c) {
      if (node[0][c] === node[1][c] && node[1][c] === node[2][c]) {
        if (node[1][c] === pro) {
          proCount += 1;
        } else if (node[1][c] === ant) {
          antCount += 1;
        }
      }
    }

    return {
      proCount: proCount,
      antCount: antCount
    };
  }

  function get3DiaCount(node, pro, ant) {
    var proCount = 0;
    var antCount = 0;

    if (node[0][0] === node[1][1] && node[1][1] === node[2][2]) {
      if (node[1][1] === pro) {
        proCount += 1;
      } else if (node[1][1] === ant) {
        antCount += 1;
      }
    }

    if (node[2][0] === node[1][1] && node[1][1] === node[0][2]) {
      if (node[1][1] === pro) {
        proCount += 1;
      } else if (node[1][1] === ant) {
        antCount += 1;
      }
    }

    return {
      proCount: proCount,
      antCount: antCount
    };
  }

  function isFull(node) {
    for (var r = 0; r < 3; ++r) {
      for (var c = 0; c < 3; ++c) {
        if (node[r][c] === 0) {
          return false;
        }
      }
    }

    return true;
  }

  function getSetCounts(node, pro, ant) {
    var rowCount = get3RowCount(node, pro, ant);
    var colCount = get3ColCount(node, pro, ant);
    var diaCount = get3DiaCount(node, pro, ant);

    return counts = {
      proSetCount: rowCount.proCount + colCount.proCount + diaCount.proCount,
      antSetCount: rowCount.antCount + colCount.antCount + diaCount.antCount
    };
  }

  function getRandomWholeNumUnder(n) {
    return Math.floor(Math.random() * n);
  }


  /* Game Nodes
  ****************************************************************************************/

  function makeIsTerminalNode(pro, ant) {
    function hasAtLeast1SetOf3(node) {
      var counts = getSetCounts(node, pro, ant);
      
      return counts.proSetCount > 0 || counts.antSetCount > 0;
    }
    
    function isTerminalNode(node) {
      return isFull(node) || hasAtLeast1SetOf3(node);
    }

    return isTerminalNode;
  }

  function makeEvaluateNode(pro, ant, randTop = 5) {
    function evaluateNode(node) {
      var counts = getSetCounts(node, pro, ant);
      
      var totalProScore = 100 * counts.proSetCount + getRandomWholeNumUnder(randTop);
      var totalAntScore = -100 * counts.antSetCount + getRandomWholeNumUnder(randTop);

      return totalProScore + totalAntScore;
    }

    return evaluateNode;
  }

  function makeGetChildNodes(first, second) {
    function getNext(node) {
      var countFirst = 0;
      var countSecond = 0;

      for (var r = 0; r < 3; ++r) {
        for (var c = 0; c < 3; ++c) {
          if (node[r][c] === first) {
            countFirst += 1;
          } else if (node[r][c] === second) {
            countSecond += 1;
          }
        }
      }

      return countFirst <= countSecond ? first : second;
    }

    function getCloneNode(node) {
      var childNode = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
      ];

      for (var r = 0; r < 3; ++r) {
        for (var c = 0; c < 3; ++c) {
          childNode[r][c] = node[r][c];
        }
      }

      return childNode;
    }

    function getChildNodes(node) {
      var next = getNext(node);
      var childNodes = [];

      for (var r = 0; r < 3; ++r) {
        for (var c = 0; c < 3; ++c) {
          if (node[r][c] !== first && node[r][c] !== second) {
            var childNode = getCloneNode(node);
            childNode[r][c] = next;
            childNodes.push(childNode);
          }
        }
      }

      return childNodes;
    }

    return getChildNodes;
  }

  
  /* Expose To World
  ****************************************************************************************/

  var isTerminalNode = makeIsTerminalNode(computer, human);
  var evaluateNode = makeEvaluateNode(computer, human);
  var getChildNodes = makeGetChildNodes(first, second);
  var maximum = 10000;

  var minimax = makeMinimax(
    isTerminalNode,
    evaluateNode,
    getChildNodes,
    maximum
  );

  function getComputerChoice(node, depth) {
    var childNodes = getChildNodes(node);
    
    if (childNodes.length < 1) {
      return {};
    }

    var score = -maximum;
    var choice = childNodes[0];

    for (var i = 0; i < childNodes.length; ++i) {
      var s = minimax(childNodes[i], depth, -maximum, maximum, false);
      
      if (s > score) {
        score = s;
        choice = childNodes[i];
      }
    }

    return {
      node: choice
    };
  }

  function getEvaluation(node, X, O) {
    var winner = 0;
    var draw = false;

    var counts = getSetCounts(node, X, O);

    if (counts.proSetCount > counts.antSetCount) {
      winner = X;
    } else if (counts.proSetCount < counts.antSetCount) {
      winner = O;
    } else if (isFull(node, X, O)) {
      draw = true;
    }

    return {
      winner: winner,
      draw: draw
    };
  }

  return {
    getComputerChoice: getComputerChoice,
    getEvaluation: getEvaluation
  };
}
