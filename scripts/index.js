/* Configuration
****************************************************************************************/

function gameNew() {
  var u = document.getElementById('game-use');
  var o = document.getElementById('game-order');
  var d = document.getElementById('game-depth');

  var human = parseInt(u.options[u.selectedIndex].value);
  var computer = human === 1 ? 2 : 1;

  var order = parseInt(o.options[o.selectedIndex].value);
  var first = order === 1 ? human : computer;
  var second = first === human ? computer : human;

  var depth = parseInt(d.options[d.selectedIndex].value);
  
  return {
    X: 1,
    O: 2,
    human: human,
    computer: computer,
    first: first,
    second: second,
    depth: depth,
    draw: false,
    winner: 0,
    elements: [
      [0, 0, 0],
      [0, 0, 0],
      [0, 0, 0]
    ]
  };
}


/* Initial State
****************************************************************************************/

var state = gameNew();

var TicTacToeFuncs = makeTicTacToeFuncs(state.human, state.computer, state.first, state.second);

drawGameCanvas(state.X, state.O, state.elements);


/* Events
****************************************************************************************/

window.addEventListener('resize', function() {
  drawGameCanvas(state.X, state.O, state.elements);
}, false);

document.getElementById('game-new').addEventListener('click', function() {
  state = gameNew();

  TicTacToeFuncs = makeTicTacToeFuncs(state.human, state.computer, state.first, state.second);

  if (state.first === state.computer) {
    var c = TicTacToeFuncs.getComputerChoice(state.elements, state.depth);

    if (c.node) {
      state.elements = c.node;
    }
  }

  drawGameCanvas(state.X, state.O, state.elements);
  displayMessage(state.X, state.O, state.winner, state.draw);
}, false);

document.getElementById('game-canvas').addEventListener('click', function(e) {
  function isOver() {
    return state.winner === state.X || state.winner === state.O || state.draw;
  }
  
  function getPositionOfClick(e) {
    var rect = document.getElementById('game-canvas').getBoundingClientRect();

    var xi = (rect.right - rect.left) / 3;
    var yi = (rect.bottom - rect.top) / 3;

    var r = Math.floor((e.clientY - rect.top) / yi);
    var c = Math.floor((e.clientX - rect.left) / xi);

    return {
      r: r,
      c: c
    };
  }
  
  function handleHumanChoice(e) {
    var p = getPositionOfClick(e);

    var r = p.r;
    var c = p.c;

    if (state.elements[r][c] !== state.X && state.elements[r][c] !== state.O) {
      state.elements[r][c] = state.human;
    } else {
      return;
    }

    var evaluationOfHuman = TicTacToeFuncs.getEvaluation(state.elements, state.X, state.O);
    
    state.winner = evaluationOfHuman.winner;
    state.draw = evaluationOfHuman.draw;

    drawGameCanvas(state.X, state.O, state.elements);
    displayMessage(state.X, state.O, state.winner, state.draw);
  }

  function handleComputerChoice() {
    var c = TicTacToeFuncs.getComputerChoice(state.elements, state.depth);
    
    if (c.node) {
      state.elements = c.node;

      var evaluationOfComputer = TicTacToeFuncs.getEvaluation(state.elements, state.X, state.O);
      
      state.winner = evaluationOfComputer.winner;
      state.draw = evaluationOfComputer.draw;

      drawGameCanvas(state.X, state.O, state.elements);
      displayMessage(state.X, state.O, state.winner, state.draw);
    }
  }

  if (isOver()) {
    return;
  }

  handleHumanChoice(e);

  if (isOver()) {
    return;
  }

  handleComputerChoice();

}, false);


/* Displaying & Drawing
****************************************************************************************/

function displayMessage(X, O, winner, draw) {
  var m = document.getElementById('game-message');

  if (winner === X) {
    m.innerHTML = 'X has won!';
  } else if (winner === O) {
    m.innerHTML = 'O has won!';
  } else if (draw) {
    m.innerHTML = 'Draw';
  } else {
    m.innerHTML = 'Tic Tac Toe';
  }
}

function drawGameCanvas(X, O, elements) {
  var canvas = document.getElementById('game-canvas');
  var context = canvas.getContext('2d');

  canvas.height = canvas.width;

  var wi = Math.floor(canvas.width / 3);
  var hi = Math.floor(canvas.height / 3);
  var we = Math.floor(canvas.width / 18);
  var he = Math.floor(canvas.height / 18);

  drawBorder();
  for (var r = 0; r < 3; ++r) {
    for (var c = 0; c < 3; ++c) {
      drawSquare(r, c);
      if (elements[r][c] == X) {
        drawX(r, c);
      } else if (elements[r][c] == O) {
        drawO(r, c);
      }
    }
  }

  function drawBorder() {
    context.lineWidth = 10;
    context.strokeRect(0, 0, canvas.width, canvas.height);
  }

  function drawSquare(r, c) {
    var w = c * wi, h = r * hi;
    context.lineWidth = 5;
    context.strokeRect(w, h, w + wi, h + hi);
  }

  function drawX(r, c) {
    var w0 = c * wi + we, h0 = r * hi + he;
    var w1 = c * wi + 5 * we + 5, h1 = r * hi + 5 * he + 5;

    context.lineWidth = 5;
    context.lineCap = 'round';

    context.beginPath();
    context.moveTo(w0, h0);
    context.lineTo(w1, h1);
    context.stroke();

    context.beginPath();
    context.moveTo(w0, h1);
    context.lineTo(w1, h0);
    context.stroke();
  }

  function drawO(r, c) {
    var w = c * wi + 3 * we + 2, h = r * hi + 3 * he + 2;

    context.lineWidth = 5;

    context.beginPath();
    context.ellipse(w, h, 2 * we, 2 * he, 0, 0, 2 * Math.PI);
    context.stroke();
  }
}
